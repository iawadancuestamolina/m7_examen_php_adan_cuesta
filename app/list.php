<?php
include_once('_header.php');
include_once('Class/ListaClass.php');
include_once('Class/ApunteClass.php');

if ($_GET['subject'] == "lengua") {
    $list = new Lista("lengua");
} else if ($_GET['subject'] == "mates") {
    $list = new Lista("mates");
} 

$coleccion = $list->get();
    ?>
<div class="row">

<table class="table">
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Documento</th>
    </tr>

    <?php
    if($coleccion != []){
    $x=1;
        foreach($coleccion as $apunte){
            ?>
<tr>
    <td><?=$x?></td>
            <td><?= $apunte->title()?></td>
            <td><button class="button"><a target="_blank" href="<?=$apunte->fileName()?>"></a></buttton></td>

</tr>        
<?php
$x++;
        }
    } else {
        ?>
                <div class="alert" role="alert">
                    <p><?="The list is empty"?></p>
                </div>
        <?php
    }
    ?>
</table>

</div>
<?php include_once('_footer.php') ?>